# CS 349 Demo Code

All the sounce code demonstrated durng lectures will be posted to this repository. As a rule, code will be posted before the lecture, hopefully the night before or earlier. You should `git clone` this  whole repo, and then `git pull` after each lecture. The code may be updated for bugs or improvements, but you'll always have the most up-to-date version if you keep pulling.

> See the [course website](https://www.student.cs.uwaterloo.ca/~cs349/f17/) for the lecture slides associated with these demos.

## The code:

* [X Windows](x/)
* [Java](java/)




---

Bugs? Missing files? Fixes? Post to [Piazza](https://piazza.com/class/j6p05nk8vs52rt) with `lectures` tag.